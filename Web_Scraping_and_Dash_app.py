# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# November 2021


################################### Example of web scraping and display a wordcloud in dash application 


# Store links
NewsFeed_AFP = "https://news.google.com/rss/search?q=source:AFP&um=1&ie=UTF-8&num=100&hl=fr&gl=FR&ceid=FR:fr"
NewsFeed_Reuters = "https://news.google.com/rss/search?q=source:Reuters&um=1&ie=UTF-8&num=100&hl=fr&gl=FR&ceid=FR:fr"
NewsFeed_Associated_Press = "https://news.google.com/rss/search?q=source:AP&um=1&ie=UTF-8&num=100&hl=en-US&gl=US&ceid=US:en"
NewsFeed_Bloomberg = "https://news.google.com/rss/search?q=source:bloomberg&um=1&ie=UTF-8&num=100&hl=en-US&gl=US&ceid=US:en"
NewsFeed_United_Press_International = "https://news.google.com/rss/search?q=source:UPI&um=1&ie=UTF-8&num=100&hl=en-US&gl=US&ceid=US:en"




# Import libraries
import feedparser
from bs4 import BeautifulSoup

# Create function to collect all news summaries
def NewsFeed_extract(newsfeed):
    NewsFeeds = feedparser.parse(newsfeed)
    tab = [NewsFeed.summary for NewsFeed in NewsFeeds.entries] # we extract what we want with feedparser
    tab_ret = []
    for link in tab: 
        soup = BeautifulSoup(link, 'html.parser') #  create beautiful soup object
        a_tag = soup.find_all('a')[0]
        if(a_tag.text!=None):
            tab_ret.append(a_tag.text) # we retrieve the html text
            
    return tab_ret




# Retrieve information for each link thanks to the previous created function
news1 = NewsFeed_extract(NewsFeed_AFP)
#print(news1)
news2 = NewsFeed_extract(NewsFeed_Reuters)
news3 = NewsFeed_extract(NewsFeed_Associated_Press)
news4 = NewsFeed_extract(NewsFeed_Bloomberg)
news5 = NewsFeed_extract(NewsFeed_United_Press_International)


# We can join all information into one object
news = news1 + news2 + news3 + news4 + news5
news = ','.join(news)






########################################## Wordcloud with dash


# Import library
import dash
import dash.dependencies as dd
from dash import dcc
from dash import html
from io import BytesIO
from wordcloud import WordCloud
import base64



# We have to run everything under all at once to make it work

# Create app
app = dash.Dash(__name__)

# Create app's layout that will contain an html image
app.layout = html.Div([
    html.Img(id= "image_wc"),
    ])

# Create function to convert object into image
# Then we generate the wordcloud with out data
def plot_wordcloud(data):
    wc = WordCloud(background_color = 'black', width = 800, height = 600).generate(data)
    return wc.to_image()

# Create callback to make the image appear on the screen
@app.callback(dd.Output('image_wc', 'src'), [dd.Input('image_wc', 'id')])
def make_image(b):
    img = BytesIO()
    plot_wordcloud(data=news).save(img, format='PNG')
    return 'data:image/png;base64,{}'.format(base64.b64encode(img.getvalue()).decode())


# Command that allows to run the dash app
if __name__ == '__main__':
    app.run_server(debug=False)

# When you run the dash app code above, you will have a link in the console  (running on http://..... 
# You can copy and paste it in a browser to have the app
# If it doesn't work you can try with debug=True

# It's an easy example but for the wordcloud we can change size, color, remove words (stopwords), ...



